
import json

print("Vamos a pasar diccionario a string con json dumps")

diccionario = {"nombre":"David", "apellido":"Pineda"}

print("TOdo el diccionario:>")
print(diccionario)

print("Solo el nombre")
print(diccionario.get('nombre'))

print("Solo el apellido")
print(diccionario.get('apellido'))

texto_dicc = json.dumps(diccionario)


print("EN texto")
print(texto_dicc)
